*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser 

Test Setup          Open Obstacle
Test Teardown       Verify Solved

*** Test Cases ***

IDS ARE NOT EVERYTHING
    [Tags]  22505
    Wait Until Page Contains Element  //a[@class="btn theme-btn-color btn-lg animated fadeInDown delay-3 btn-big"]
    Click Link                        //a[@class="btn theme-btn-color btn-lg animated fadeInDown delay-3 btn-big"]
    Verify Solved
