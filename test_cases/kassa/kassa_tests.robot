*** Settings ***
Resource            kassa_resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser

Test Setup          Preparation
Test Teardown       Verify Solved

*** Test Cases ***

FILL_CUSTOMER_INFORMATION
    Wait Until Element Is Enabled    	id:first-name		5s
    Input Text  			id:first-name 		Timo		true
    Input Text                      	id:last-name		Vilen		true
    Input Text                          id:postal-code		39130		true
    Wait Until Element Is Enabled    	id:continue		5s
    Click Element                       id:continue


