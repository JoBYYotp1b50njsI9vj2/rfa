*** Settings ***
Library           SeleniumLibrary   run_on_failure=Nothing

*** Variables ***
${SERVER}         https://www.saucedemo.com/
${BROWSER}        Firefox
${DRIVER}         geckodriver
${DELAY}          0

*** Keywords ***
Prepare Browser
    Open Browser    ${SERVER}    ${BROWSER}   executable_path=${DRIVER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Preparation
    Go To   ${SERVER}
    Wait Until Page Contains Element   id:user-name
    Input Text   id:user-name  standard_user  true
    Input Text   id:password  secret_sauce  true
    Click Button   id:login-button

Next Test
    Click Element   id:react-burger-menu-btn
    Click Link   id:reset_sidebar_link
    Click Link   id:inventory_sidebar_link
