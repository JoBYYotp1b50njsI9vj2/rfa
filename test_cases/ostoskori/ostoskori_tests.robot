*** Settings ***
Resource            resource.robot

Suite Setup         Prepare Browser
Suite Teardown      Close Browser

Test Setup          Preparation
Test Teardown       Next Test

*** Test Cases ***

ADD_SINGLE
    Wait Until Page Contains Element    id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-backpack
    Go To   ${SERVER}cart.html
    Page Should Contain   Sauce Labs Backpack

ADD_MULTIPLE_DIFFERENT
    Wait Until Page Contains Element    id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-bolt-t-shirt
    Go To   ${SERVER}cart.html
    Page Should Contain   Sauce Labs Backpack
    Page Should Contain   Sauce Labs Bolt T-Shirt

ADD_MULTIPLE_SAME
    Wait Until Page Contains Element    id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-backpack
    Go To   ${SERVER}cart.html
    Element Should Contain   class:cart_quantity   2

REMOVE_ITEM_FROM_CART
    Wait Until Page Contains Element    id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-backpack
    Go To   ${SERVER}cart.html
    Page Should Contain   Sauce Labs Backpack
    Click Element                       id:remove-sauce-labs-backpack
    Page Should Not Contain   Sauce Labs Backpack

REMOVE_ITEM_FROM_INVENTORY
    Wait Until Page Contains Element    id:add-to-cart-sauce-labs-backpack
    Click Element                       id:add-to-cart-sauce-labs-backpack
    Go To   ${SERVER}cart.html
    Page Should Contain   Sauce Labs Backpack
    Go To   ${SERVER}inventory.html
    Wait Until Page Contains Element    id:remove-sauce-labs-backpack
    Click Element                       id:remove-sauce-labs-backpack
    Go To   ${SERVER}cart.html
    Page Should Not Contain   Sauce Labs Backpack
